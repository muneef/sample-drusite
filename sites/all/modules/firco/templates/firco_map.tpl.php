<div id="firco-map">
	<div class="container">
    <h4>WE REACH PEOPLE ALL OVER THE WORLD</h4>
  	<h1>Fuelled by a community of developers, we are taking over</h1>

  	<?php

  	$path = drupal_get_path('theme','fitco');
  	
  	$variables = array(
        'path' => $path. '/img/map-homepage@2x.png', 
        'alt' => 'Test alt',
        'title' => 'Test title',
        'attributes' => array('class' => 'img-responsive'),
      );
    	
    	$img = theme('image', $variables);

    	print $img;

    ?>
  </div>
</div>