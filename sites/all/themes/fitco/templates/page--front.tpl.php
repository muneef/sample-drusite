<div class="container">
	<header id="menu-master">
	  <div class="col-md-2">
	    <h3>
	    	<?php
				$site_name = variable_get('site_name');
				$site_slogan = variable_get('site_slogan');

				print $site_name."  |  ".$site_slogan;
			?>
		</h3>
	   </div>
	   <div class="col-md-10">
	    <?php print render($page['header']); ?>    
	  </div>  
  	</header>
</div>


<div id="featured">	
	<?php print render($page['featured']); ?>
</div>

<footer>
	<div class="container">
		<?php print render($page['footer']); ?>
	</div>
</footer>
