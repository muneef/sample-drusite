<div class="container">
	<header id="menu-master">
	  <div class="col-md-2">
	    <h3>My Page</h3>
	   </div>
	   <div class="col-md-10">
	    <?php print render($page['header']); ?>
	  </div>  
  	</header>
</div>

<div class="container">
	<?php print render($page['content']); ?>
</div>

<footer>
	<div class="container">
		<?php print render($page['footer']); ?>
	</div>
</footer>
